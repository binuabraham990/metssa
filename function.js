function selection()	{
	interval = 20;
}

selection.prototype.submit_fn = function()	{
	musicSelected = $('input[name=music]:checked').val();
	musicVolumeVal = $("#musicVolume").val()/100;	
	disturbVolumeVal = $("#disturbVolume").val()/100;
	interval = $("#interval").val();
	//alert(interval);
	//alert(musicVolumeVal);
	player1 = new Media("/android_asset/www/media/"+musicSelected+".ogg",ob.onSuccess,ob.onError,ob.onStatus);
	player1.play();
	player1.setVolume(musicVolumeVal);
	//player1.loop();

	ob.selectDisturbence();
	var milliInterval = interval*1000;
	intervalid = setInterval(ob.calcChace, milliInterval);

	/* Take value of toggle button 'stopAfter' and set whether pgm finish automatically after selected time */
	var stopAfterTime = $("#stopAfter").val();
	if(stopAfterTime == "on")	{
		var timeToRun = timeSlider*60*1000;
		var intervalidStopAfterTime = setInterval(ob.cancel_fn,timeToRun);
	}
	/*----------------------------------------------------------------------*/

	ob.disableAll();
}

selection.prototype.cancel_fn = function()	{
	ob.enableAll();
	if(player1)	{
		player1.stop();
	}

	player2 = new Media("/android_asset/www/media/1.ogg");
	player2.stop();
	player2.setVolume(disturbVolumeVal);
	clearInterval(intervalid);
}
//suresh 8113016111
//pix solutions

selection.prototype.calcChace = function()	{
	timeSlider = $("#timeSlider").val();			// Taking value of timeSlider
	countSlider = $("#countSlider").val();			// Taking value of noiceChance
	var actualCount = (timeSlider*(60/interval)*countSlider)/100;	
	/* For 10 min in timeSlider and 50% in countSlider this will produce (30*50)/100 = 15 for an interval 20. */

	var totalTime = timeSlider*(60/interval);				// 
	var ran = Math.random()*totalTime;						

	if(ran > 0.5 && ran < actualCount + 0.5)	{
		var ranDisturbence = Math.floor(Math.random()*disturbences.length);
		//alert(disturbences[ranDisturbence]);
		if(player2)	{
			player2.stop();
		}
		player2 = new Media("/android_asset/www/media/"+disturbences[ranDisturbence]+".ogg");
		player2.play();
		player2.setVolume(disturbVolumeVal);						// 
	}
}

selection.prototype.selectDisturbence = function()	{
	var id = 0;
	for( var i = 1; i <= 8; i++)	{
		if ($('#checkbox'+i).is(":checked"))	{
			disturbences[id] = i;
			id++;
		}
	}
}

selection.prototype.disableAll = function()	{
	/* Used to disable all forms in the interface when play button clicked...*/
	$("input[type='radio']").checkboxradio('disable');
	$("input[type='radio']").checkboxradio("refresh");
	$("input[type='checkbox']").checkboxradio('disable');
	$("input[type='checkbox']").checkboxradio("refresh");
	$("#timeSlider").slider('disable');
	$("#timeSlider").slider('refresh');
	$("#countSlider").slider('disable');
	$("#countSlider").slider('refresh');
	$("#musicVolume").slider('disable');
	$("#musicVolume").slider('refresh');
	$("#disturbVolume").slider('disable');
	$("#disturbVolume").slider('refresh');
	$("#submit").button('disable');
	$('#submit').button('refresh');
	$('#cancel').button('enable');
	$('#cancel').button('refresh');
}

selection.prototype.enableAll = function()	{
	/* Used to enable all forms in the interface when stop button clicked...*/
	$("input[type='radio']").checkboxradio('enable');
	$("input[type='radio']").checkboxradio("refresh");
	$("input[type='checkbox']").checkboxradio('enable');
	$("input[type='checkbox']").checkboxradio("refresh");
	$("#timeSlider").slider('enable');
	$("#timeSlider").slider('refresh');
	$("#countSlider").slider('enable');
	$("#countSlider").slider('refresh');
	$("#musicVolume").slider('enable');
	$("#musicVolume").slider('refresh');
	$("#disturbVolume").slider('enable');
	$("#disturbVolume").slider('refresh');
	$("#submit").button('enable');
	$('#submit').button('refresh');
	$('#cancel').button('disable');
	$('#cancel').button('refresh');
}

selection.prototype.onSuccess = function()	{			
	// Nothing do...		
}

selection.prototype.onError = function()	{
	// Nothing do...		
}
	
selection.prototype.onStatus = function(status)	{
	if(status==Media.MEDIA_STOPPED)	{
		media.play();
	}
}

selection.prototype.submitOption = function()	{
	
}
